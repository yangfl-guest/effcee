#!/bin/sh
set -e

mkdir build
cd build
cmake ..
make
make test
cd ..
rm -rf build
